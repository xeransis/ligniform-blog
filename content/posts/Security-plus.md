---
title: "My Security+ Journey"
date: 2024-01-28
# weight: 1
tags: ["cybersecurity", "learning"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "My process I followed to pass the Comptia Security+ (SY0-601)"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

---

I recently passed the Security+ (SY0-601).
This is my first InfoSec related ceritification, so I thought I might write a bit about what I learned along the way.  

---


Way back in April of 2023 I was on a month long trip around Europe.

I already knew by that point that I wanted to pivot from the helpdesk to the Security team at my current company. I had expressed my interest, but wanted to show that I was willing to learn outside of work.

The Security+ is an entry level certificate. It covers the basic knowledge you need to get into InfoSec as a career (In my opinion. You can get a job without any qualifications to be honest).

When I started studying I had a surface level understanding of most of the topics.

I knew what Worms, Trojan-horses, and ransomware were. OSINT and threat intel were fun. In fact they were something I was already doing before considering cybersecurity as a career.
I had heard that the Security+ was a good starting point, so I decided to look into it.

The exam objectives are listed below;
- Attacks, Threats, and Vulnerabilities
- Architecture and Design
- Implementation
- Operations and Incident Response
- Governance, Risk, and Compliance

The main topic I didn't know about was "Governance, Risk, and Compliance". Writing policies seems pretty boring to me. Maybe that'll change one day, but I prefer hands on analysis to anything else.

So, instead of reading the exam objectives or [paying $565 for the 'self paced study guide'](https://www.comptia.org/training/books/security-sy0-601-study-guide), I just searched up online courses.

YouTube was the first place I went. As as web-developer I found YouTube videos a lot better at teaching than an 8 hour Udemy course. Plus YouTube series are free, which is nice.

The first series I found was [Professor Messer's course](https://www.professormesser.com/security-plus/sy0-601/sy0-601-video/sy0-601-comptia-security-plus-course/).

This is a series that goes through each exam objective (There are 100s in the 601, it's hell) in great detail. It's simple to understand, and helped me out with the snore-fest that is 'Governance, Risk, and Compliance'.

I almost exclusively used this course by the way. I'll touch on some of the other things I used, but if you just use this course you should be fine (Did I mention it was free?).

The course is long and I was busy with work (As an analyst now) and life. Soon enough I put the exam to the side.

At work I was getting hands on experience with Phishing attacks, making EDR rules, and pretty much everything else covered on the exam topics (Except the dreaded/boring Governance part). I assumed that by the time I took the exam I would be *fine*.

Eventually I knew I had to sit the exam. You can do all the study in the world for something, but eventually you need to do the actual exam (Kind of, more on this later).

I made a post on Mastodon asking for any resources anyone had in mind. Mastodon is an incredible community. Everyone is lovely and open to infosec noobs like myself.
One of the replies I got was from [Tinker](https://infosec.exchange/@TInker), who also suggested ProfessorMesser's videos.

I made the decision to set a date for my exam. One week from then. Without a set date I would just keep putting it off.

I started cramming the videos, a few podcasts, and I even started reading into the hellscape that is Governance, Risk, and Compliance (It's honestly not that bad, just doesn't interest me.)

After payday I had enough for the exam. It cost more than my first car, but I was somewhat confident about taking the exam.
![My first car. A white, two door sports car. It has a weird blue 80s/90s wave pattern on the side. The picture is taken on a grassy hill. In the background there is farmland, with some sheep visible. The sky is gold, it was taken in the morning, or in the evening.](/static/car.png)

The last thing I did was look up exam questions.

I know, technically I shouldn't have. I [wrote previously](/posts/stack-overflow) about how having answers given to you doesn't teach you anything. But it was the night before the exam, and I was feeling nervous as hell.

None of the questsions I crammed actually showed up on the exam. They moreso taught me the style in which the questions would be written. A lot of the questions try to catch you out in how weirdly they're written. They might sound like they're talking about ransomware, for example. But the last line will mention how the virus is self-propogating (So, a worm.).

### PearsonVue

Alright, time to rant a little. PearsonVue is horrible.

They let you sit exams from the comfort of your own home. Pretty much any time during the day (and night) is available. This is all great.

Their customer service, and the spyware-like app you need to install to sit the exam is horrible. The program you take the exam through will close if it detects MS-Teams, Discord, Signal, any VOIP app pretty much. There are [plenty](https://www.reddit.com/r/aws/comments/fscq7v/psa_dont_take_remote_exams_offered_by_pearson_vue/) [more](https://www.reddit.com/r/AWSCertifications/comments/p2wwil/pearson_vue_online_proctored_experience_is/)  [examples](https://www.reddit.com/r/sysadmin/comments/t61dry/pearson_vue_are_the_literal_worst_company_i_have/) [online](https://www.reddit.com/r/CompTIA/comments/ofm3gf/advice_when_taking_pearson_onvue_online_exams/) of PearsonVUE being hard to deal with. I don't need to detail them all here.

I had to use my partners Windows laptop to run my exam. This was mainly my fault. PearsonVUE doesn't support Linux, not many things do. I just happen to use Linux on all my computers.

---

### Final thoughts.

I passed, by the way. The passing score is 750, and I passed with 777.
I like to think that I studied only as much as I needed to. During the exam I assumed I would fail. I was surprised when I passed, but glad it's finally over.

Use [Professor Messer's course](https://www.professormesser.com/security-plus/sy0-601/sy0-601-video/sy0-601-comptia-security-plus-course/). It's seriously the best thing I stumbled across, forgot, then was reminded of. It's free, easy to understand (In English), and covers only what is on the exam. There's no bullshit around trying to sell you a course or subscription. It's just high quality learning.

Big thanks to [Tinker](https://infosec.exchange/@TInker) once again for reminding me of the above course. Also for getting me into SolarPunk stuff! I'm planning on writing about my journey into that too!

My next exam is the [SC-200](https://learn.microsoft.com/en-us/credentials/certifications/exams/sc-200/). I'll probably write about that too.
