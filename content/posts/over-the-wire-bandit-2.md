---
title: "Over the Wire - Bandit (Part 2)"
date: 2024-10-06
# weight: 1
tags: ["Privacy"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: true
hidemeta: false
comments: false
description: "Back on the grind"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

---
We're so back baby.

When I [last left off](/over-the-wire-bandit) I had just finished up with level 17. Let's get back into it!

Small aside:
When I was trying to resume this I was having some issues. Turns out [Level 0➡️1](https://overthewire.org/wargames/bandit/bandit1.html) mentions that *'Passwords also occassionally change. It is recommended to take notes on how to solve each challenge'*. In preparation for this write up I had to do every level all over again.

---
### [Level 16➡️17](https://overthewire.org/wargames/bandit/bandit17.html)
> The credentials for the next level can be retrieved by submitting the password of the current level to a port on localhost in the range 31000 to 32000. First find out which of these ports have a server listening on them. Then find out which of those speak SSL/TLS and which don’t. There is only 1 server that will give the next credentials, the others will simply send back to you whatever you send to it.

`nmap` time baby.

I love using nmap. It's a super useful little tool that I use at work (Not that I *actually* need to. I don't do any offensive stuff (yet)).

It's been a while since I've done this. It took some re-reading (and installing `ssh`, thanks Arch!) to realize what I actually have to do.



