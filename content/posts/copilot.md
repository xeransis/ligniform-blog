---
title: "Copilot is a privacy nightmare."
date: 2024-06-16
# weight: 1
tags: ["tech", "privacy", "microsoft"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: true
hidemeta: false
comments: false
description: "Overview of the dumpster fire that was Copilot"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

cover:
    image: "</assets/gardening-tools.jpg>" # image path/url
    alt: "<A variety of Garden tools. Brooms, rakes, shovels etc in a pot or something>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---


