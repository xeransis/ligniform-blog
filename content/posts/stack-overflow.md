---
title: "StackOverflow is the best tool I don't use"
date: 2024-01-06
# weight: 1
tags: ["programming", "learning"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Learning can be  hard"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

cover:
    image: "</assets/gardening-tools.jpg>" # image path/url
    alt: "<A variety of Garden tools. Brooms, rakes, shovels etc in a pot or something>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---

My first job outside of uni (College for the Americans) was as a developer.  
Originally I wanted to be a video game developer. I planned to get my diploma (Not a degree, maybe a blog post for another time!) and go straight into game development.  
I fell in love with scripting and making software. The game development industry seemed chaotic and lost it's appeal, so I stuck to software development and got myself a job. 

As almost all developers will know [StackOverflow](https://stackoverflow.com/) is an essential tool. It described as so:
> Stack Overflow is a question-and-answer website for computer programmers. It features questions and answers on certain computer programming topics. It was created to be a more open alternative to earlier question and answer websites such as Experts-Exchange.

If you ever need to convert a float to a string (for example), you can very quickly find [someone from 12 years ago](https://stackoverflow.com/questions/7552660/convert-float-to-string-and-string-to-float-in-java) asking the same thing.  
My problem with StackOverflow is my own. It's an amazing site if you want a quick answer, but therein lies my main issue.  
For me, if I was stuck on an issue I would search the error message, find a similar StackOverflow question, and get an answer. Great!

But what did I *learn*?

Well, nothing. If I ran into the same issue again, I would go back to searching it. I wouldn't actually *learn* anything.  
I knew I could just search my issue up and get an answer. If I couldn't find an answer straight away I would keep digging, or ask it myself.

When you're just given the answer you don't really learn how or why it solves your issue. You just know it **does**. This goes well until you start using something incredibly new (or ancient). 

## RTFD
My co-worker would always tell me to 'Read the fucking docs'. It's something I heard a lot over the years and never really thought about.

Turns out it's **really good advice.**

Getting the answer on StackOverflow is quick and easy, but reading the documentation will make you understand. Once I started reading documentation I started understanding what I could actually start doing with a language.  
I understood how things started working. I felt like a much better developer when I could read an error message, and immediately know what I did wrong. I still had some pretty bad [imposter syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome) but I felt more confident in myself. 

Nowadays whenever I have an issue I look for the documentation. It might take longer but I feel like I'm actually learning, rather than just being given the answer. StackOverflow is an amazing tool, but I try not to use it. 

***
That's it! This is my first blog post. I'd like to give a big thanks to [Jean](https://infosec.exchange/@imajeanpeace) for inspiring me to finally write this. I'm excited to write more and finally get stuff out there.  
Feel free to follow me on [Mastodon](https://infosec.exchange/@ligniform). Lots of love. 
