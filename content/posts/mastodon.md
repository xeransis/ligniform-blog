---
title: "One year on Mastodon"
date: 2024-04-25
# weight: 1
tags: ["Infosec"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Reflecting on my year on Mastodon (Or on Infosec.Exchange anyway)"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

---
Wowza, already been a year huh?

It's actually been just *over* a year since I joined [Infosec.exchange](https://infosec.exchange). I was on [Mastodon.Social](https://mastodon.social) for a while actually, but moved across a few months later.

This is just a quick post I wrote in an hour or two, Looking back on my experience so far.

## Twitter
I joined Twitter sometime in 2016. I had a few content creator friends, and at the time I wanted to work in video games. I was at high-school and I wanted to find an online community.
Twitter *was* the place for that! I connected with a few people and got a few followers.
I fell into 'Web Dev' Twitter. There were a few 'big players' who everyone seemed to follow, and they all seemed to interact with each other. It was nice, but I felt that my follower count (A whole 80!) made me feel like I couldn't interact with these big accounts.

I didn't feel like I was a part of a community. It felt more like I was an outsider. I'm sure some of that came from being an anxious guy, but follower counts really seemed to matter.

I still spent most time on my phone scrolling through Twitter. It seemed like any film news, local news, random memes, or random drama sprouted there. It felt like I knew what was going on, but still just felt like I was watching on.

I thought it was neat when some verified people followed me. They were working the video game industry, so I thought that I had a good way into the industry. People were posting their cool [Unity](https://unity.com/)/[Unreal](https://www.unrealengine.com) projects, and I felt like when I eventually started to make things I'd be in a good place.

Eventually I started finding the Video game industry boring. I prefered making scripts that do one thing well, rather than a whole complex system. The 'Web Dev' community I was a part of seemed to have good guides, so I felt like I was in a good place.


## Moving to Mastodon
I moved to Mastodon soon after Twitter was in the talks to be sold off. I didn't want to be on a platform ran by Elon Musk. I already didn't like the guy and I could see weird changes coming.

As I mentioned above, I moved to Mastodon.Social to begin with. It was the largest instance, so I thought it would be a good way to tell what Mastodon was all about. This was also the time where I was getting disallusioned with software development. I was stressed from work and I didn't know what to do.
Everyone on Mastodon seemed chill. I liked that favorites and boosts weren't (really) that obvious. It made me feel like I could interact with any post I saw. When I did start interacting with posts I was surprised how common it was for someone to interact back.
Mastodon felt like more of a community than Twitter ever was. People were posting interesting things, and I quickly gained well over the amount of followers I had back on Twitter.

Accessibility and inclusivity seemed to be a big focus. People who would be relentlessly mocked on Twitter had a home on Mastodon (This is from what I've seen. I know there's some issues). It was cosy.

## Infosec time
I moved to Infosec.Exchange in April.
I was at a stage in my life where I wanted to get a job in InfoSec and was studying hard for my [Security+](/posts/security-plus). Infosec.Exchange was the biggest InfoSec adjacent instance, so I decided to move over.
It was fun! I started interacting with names I recognised. I made some friends and felt like I was finally a part of a community, rather than just watching one from the outside.

InfoSec is fun. The people in InfoSec are cool, and I love being a part of the community.

I'm sitting at 407 followers at the moment. I don't give too much weight to numbers, but it's nice getting interactions on my own posts now. I feel like I'm a part of a nice little hacker community now. It's great.

## Conclusion
Not much to say here. This wasn't a big breakdown about why Twitter is bad. I just like Mastodon, a lot.

Thanks to everyone who follows me or interacts with my stuff. Thanks to [Jerry](https://infosec.exchange/@jerry) too, I was never going to pay for Twitter but I happily throw money towards the InfoSec shitposting site.

Thanks for reading
