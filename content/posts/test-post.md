---
title: "TEST POST"
date: 2020-09-15T11:30:03+00:00
# weight: 1
# aliases: ["/TEST"]
tags: ["post", "first"]
author: "Ligniform"
showToc: true
TocOpen: false
draft: true
hidemeta: false
comments: false
description: "Test post. Don't mind this"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

cover:
    image: "<image path/url>" # image path/url
    alt: "<alt text>" # alt text
    caption: "<text>" # display caption under cover
    relative: false # when using page bundles set this to true
    hidden: true # only hide on current single page
---
