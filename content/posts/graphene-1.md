---
title: "GrapheneOS - One year in"
date: 2024-01-23
# weight: 1
tags: ["tech", "learning", "privacy"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: false
hidemeta: false
comments: false
description: "Quick thoughts, one year into using GrapheneOS"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

---

I had heard about [GrapheneOS](https://grapheneos.org/) a few years ago. I was getting into personal privacy and wanted to stop feeding all my info to big tech.  
My problem at the time was that [only Google Pixel devices are supported](https://grapheneos.org/build#build-targets) (Officially at least).  
I was a high-school student at the time, so the small amount of money I got from my part-time job mainly went to video games.  

Privacy could wait.  

Skip forwards a few years and I finally wanted to take privacy seriously. I was (legally) an adult and I wanted to own what personal data of mine was shared. Once again I looked into GrapheneOS and decided it was time.  

I decided to get a brand new Pixel. The second hand mobile phone market in my country is a bit light. Any recent Pixel that was available went for a similar, if not higher price than brand new.  

### Installation
Much like with my recent PC purchase it was only turned on for a moment before I tore the stock OS out and installed the new one.  
GrapheneOS is incredibly easy to install. I used the [web installer](https://grapheneos.org/install/web), which was shockingly easy to run.  
After enabling OEM unlocking and plugging the phone in I just clicked on the buttons in order, and in a few minutes I had GrapheneOS up and running.  

I want to quickly point out that while I've not heard of many issues installing GrapheneOS (Unlike with Arch Linux), I'm sure there can be some issues.  
Thankfully the [GrapheneOS discussion forum](https://discuss.grapheneos.org/) is pretty active and people seem willing to help out. 

### Apps

Once I had Graphene installed I wanted to install some apps. As most people do when getting a new phone.  
I had already used [F-Droid](https://f-droid.org/) as an alternative app store on my previous phone, so the first thing I did was open [Vanadium](https://grapheneos.org/features#vanadium) (A hardened version of chromium that comes pre=installed) and download it.  
For some of the below apps I actually installed the APK from their own websites. F-Droid makes this quite easy, with most apps you can install through F-Droid, or just download the APK and do it manually.  

Here are some of the apps I installed, and what they do:
- [OsmAnd~](https://osmand.net/)
	- This is a FOSS alternative to Google Maps. OpenStreetMap is widely used and very reliable. I've had some issues such as finding exact addresses, but for the most part it's done an amazing job. 
- [ProtonMail, ProtonCalendar, and ProtonVPN](https://proton.me/)
	- I don't like having a VPN service and a Mail service under the same company, but Proton's offerings are really nice. Easy move from Gmail to Proton for me. 
- [Lichess](https://lichess.org/)
	- FOSS Chess. Not much more to say. I just like playing chess. 
- [NewPipe](https://newpipe.net/)
	- NewPipe is a great (FOSS) YouTube, PeerTube, SoundCloud, and Bandcamp client. It allows picture-in-picture, local downloads, and most importantly, ad-free experiences of all four services. 
- [Signal](https://signal.org/)
	- This E2EE messaging service is what I use day to day. If you care about privacy you've probably already heard of it. It's great!
- [FireFox](https://www.mozilla.org/en-US/firefox/)
	- Vanadium is fine. But it's still just a chromium fork. I've used FireFox for years and prefer using it. 
- [KeePassDX](https://www.keepassdx.com/)
	- KeePass is a password manager. I keep my password vault hosted locally on each of my devices, syncing them monthly (Though I probably need to do this more often). I don't trust cloud-based password managers at all, and KeePassDX hasn't let me down yet. 
- [Aegis](https://github.com/beemdevelopment/Aegis)
	- Aegis is a FOSS MFA app. I use MFA on all my accounts and Aegis is right there with me. 
- [Termux](https://termux.dev/en/)
	- Termux is a terminal emulator. It lets me SSH into things, even write parts of this blog, all from my phone. There's plenty you can do with it that I don't even know about. Give it a shot.   

That's it. No more apps installed on my phone.  
One of the major criticisms I heard of GrapheneOS was that popular banking apps don't work. Fortunately for me, my bank has a webapp that works just fine. No issues there.  
For a lot of the apps I used on my old phone, I could easily replace with a webapp. Discord, Mastodon, BookWyrm, PixelFed, and a few others all work perfectly.  
If you're planning on moving to GrapheneOS I highly recommend using webapps over installing an app.  


### Weird stuff

#### Notifications 
GrapheneOS is hardened. It's privacy focused, which is great! It just means there are some odd things that you might come across.  
Notifications was a big one for me. By default any installed app will use 'Optimized' battery usage settings. This meant that I wouldn't get any notifications, which was pretty annoying.  
Thankfully you can fix it easily. Going to Settings > Apps > (Select your app) > App Battery Usage. Then just change it to unrestricted. Easy peasy.  

#### File permissions
You can configure storage scopes for each app. This works nicely to segment your data.  
My only problem with this is that I like saving memes from Mastodon and sending them to people on Signal, or through Discord (For my friends who still use it).  
Eventually I gave up using storae scopes and saved everything to my 'downloads' folder. Or somewhere in the Android file structure.  

#### Headphones
I use wireless headphones most of the time. It actually took me a while to realize that the model of Pixel I bought doesn't have a headphone jack.  
Apple removed the headphone jack of the iPhone 7 back in 2016. Slowly it just became normal. It sucks.  


### Final thoughts
That about wraps it up. I love my Pixel. GrapheneOS is very fun to use. I love reclaiming my privacy, one small step at a time. 

***
Thanks for reading! Check me out [on Mastodon](https://infosec.exchange/@ligniform) for my insane InfoSec ramblings. Lots of love <3 
