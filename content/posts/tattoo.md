---
title: "Tattoos and Threat Models"
date: 2024-02-06
# weight: 1
tags: ["Privacy"]
author: "Ligniform"
showToc: false
TocOpen: false
draft: true
hidemeta: false
comments: false
description: "Thoughts on Tattoos and Privacy"
disableHLJS: true # to disable highlightjs
disableHLJS: false
disableShare: true
hideSummary: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowWordCount: true
UseHugoToc: true

---
Alright, this took a long time to write.   

I love privacy. It's [a human right](https://www.ohchr.org/en/privacy-in-the-digital-age/international-standards) after all.  
I've also recently started to love tattoos. I've always wanted 

#Conclusion
As I mentioned at the top of this post; This took a long time to write.  
I'm privacy-focused, but in the big picture my threat model isn't too high.  
I'm not in a privacy 'unfriendly' country, I'm not a member of a targetted group, and I'm not on the run from any governments (Yet). 

